package com.example.denis.pokemonapplication

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val lat = CameraViewActivity.TARGET_LATITUDE
        val lng = CameraViewActivity.TARGET_LONGITUDE
        // Add a marker in Sydney and move the camera
        val pObject = LatLng(lat, lng)
        mMap.addMarker(MarkerOptions().position(pObject).title(getString(R.string.p_name)).draggable(false))

        val cameraPosition = CameraPosition.Builder()
                .target(pObject)
                .zoom(15f)
                .build()

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

    }

    override fun onResume() {
        super.onResume()
        if (!isOnline(this)){
            Toast.makeText(this, R.string.connect_inthrnet, Toast.LENGTH_LONG).show()
            networkSettings()

        }
    }
    private fun isOnline(context: Context): Boolean {
        var network=false
        var cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var netInfo = cm.activeNetworkInfo

        if (netInfo !=null && netInfo.isConnectedOrConnecting ){
            network=true
        }
        return network
    }
    private fun networkSettings() {
        startActivity( Intent(Settings.ACTION_SETTINGS))
    }
}
