package com.example.denis.pokemonapplication.recyclerViewTask

import android.content.Context
import android.content.Intent
import android.content.res.AssetManager
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.denis.pokemonapplication.R
import com.example.denis.pokemonapplication.TaskActivity
import kotlinx.android.synthetic.main.task_view.view.*
import android.graphics.Typeface



class TaskAdapter(var data:ArrayList<QestTask>, private var context: Context,var taskStatusList:ArrayList<String>)
    :RecyclerView.Adapter<TaskAdapter.TaskViewHolder> (){

    var taskString = "Задание "
   // var twoDotString=": "
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.task_view,parent,false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int ) {

        //var than="font/thannhaeuser.ttf"
       // val typeface = Typeface.createFromAsset(context.assets,than)
        holder.taskNumber.text=taskString+data[position].taskId.toString()
        holder.taskStatus.text="Недоступно"

        var completedQest = "0"


        if (data[position].taskId.toString() == (taskStatusList.size + 1).toString()){
            /* берем последнее значение+1 (следующее задание которое будет выполняться) из массива выполненых и сравниваем с id задания */
            holder.taskStatus.text="Выполняется"
            //метод на клик
            makeClick(position, holder)
        }

        for (i in 0 until taskStatusList.size ){

            if (data[position].taskId.toString() == taskStatusList[i]){
                holder.taskStatus.text=data[position].taskTitle
                completedQest="1"
                //метод на клик
                makeClick(position, holder)
            }
        }



        // вывести данные во вью и передать дальше в активити. сделать для разных заданий активитти
    }

    private fun makeClick(position: Int, holder: TaskViewHolder) {
        if (data[position].taskType == "gps") {
            holder.view.setOnClickListener {

                var intent = Intent(context, TaskActivity::class.java)
                intent.putExtra(TaskActivity.task_Id, data[position].taskId.toString())
                intent.putExtra(TaskActivity.task_Type, data[position].taskType)
                intent.putExtra(TaskActivity.ln_lng, data[position].lnlng)
                intent.putExtra(TaskActivity.task_title, data[position].taskTitle)
                intent.putExtra(TaskActivity.task_Text, data[position].taskText)
                intent.putExtra(TaskActivity.task_photo, data[position].photo)

                intent.putExtra(TaskActivity.dataWebPageKey,data[position].dataWebPage)

                context.startActivity(intent)
            }
        }
    }

    class TaskViewHolder(var  view: View): RecyclerView.ViewHolder(view){
        var taskNumber:TextView = view.findViewById(R.id.task_number_text_view)

        var taskStatus :TextView= view.findViewById(R.id.task_status_text_view)
       // var startButton:Button=view.findViewById(R.id.start_task_button)

    }
}