package com.example.denis.pokemonapplication.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.denis.pokemonapplication.recyclerViewTask.QestTask

interface ReadFileFromDiskInterface: MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun writeTextInTextView(string:String)

    @StateStrategyType(SkipStrategy::class)
    fun qestListForRecyclerView(list :ArrayList<QestTask>, taskStatusList:ArrayList<String>)

    @StateStrategyType(SkipStrategy::class)
    fun messageError()

    @StateStrategyType(SkipStrategy::class)
    fun reloadAfterDeleteData()
}