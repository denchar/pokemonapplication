package com.example.denis.pokemonapplication.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface BeforeMainActivityInterface:MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun weitSomeTimeAndStart(string:String)
}