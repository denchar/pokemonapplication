package com.example.denis.pokemonapplication


import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.denis.pokemonapplication.presenter.BeforeMainActivityPresenter
import com.example.denis.pokemonapplication.view.BeforeMainActivityInterface
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_before_main.*


class BeforeMainActivity : MvpAppCompatActivity(),BeforeMainActivityInterface {

    @InjectPresenter
    lateinit var presenter:BeforeMainActivityPresenter
    companion object {
        var pageContent="page_content"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_before_main)

       askPermission(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION){

            //all of your permissions have been accepted by the user
        }.onDeclined { e ->
            endApp()
            //at least one permission have been declined by the user
        }

    }

    override fun onResume() {
        super.onResume()
        if (!isOnline(this)){
            Toast.makeText(this, R.string.connect_inthrnet, Toast.LENGTH_LONG).show()
            networkSettings()

        }else{
            presenter.weitSomeTimeAndStart()

        }
    }


    override fun weitSomeTimeAndStart(string:String) {
        var intent = Intent(this,MainActivity::class.java)
        intent.putExtra(pageContent,string)
        startActivity(intent)
        finish()
    }


    fun isOnline(context: Context): Boolean {
        var network=false
        var cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var netInfo = cm.activeNetworkInfo


        if (netInfo !=null && netInfo.isConnectedOrConnecting ){
            network=true
        }
        return network

    }
    private fun networkSettings() {
        //
        startActivity( Intent(android.provider.Settings.ACTION_SETTINGS))
    }
    fun endApp() {
        Toast.makeText(this, R.string.permission_for_app, Toast.LENGTH_LONG).show()
        finish()
    }
}
