package com.example.denis.pokemonapplication

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

import kotlinx.android.synthetic.main.activity_data_web_view.*

class DataWebViewActivity : AppCompatActivity() {

    lateinit var webView: WebView
    companion object {
        const val dataWebViewActivityCode=1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_web_view)
        //setSupportActionBar(toolbar)

        webView=findViewById(R.id.web_view_data)
        webView.webViewClient = MyWebViewClient()
        webView.settings.javaScriptEnabled


        if (savedInstanceState == null)
        {
            webView.loadUrl(intent.getStringExtra(TaskActivity.dataWebPageKey))
        }

        fab.setOnClickListener {

            setResult(dataWebViewActivityCode)
            finish()
        }
    }

    class MyWebViewClient: WebViewClient()
    {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }


        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.url.toString())
            return true
        }
    }
    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webView.restoreState(savedInstanceState)
    }
}
