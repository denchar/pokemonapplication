package com.example.denis.pokemonapplication.presenter

import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.denis.pokemonapplication.BeforeMainActivity
import com.example.denis.pokemonapplication.R
import com.example.denis.pokemonapplication.model.BeforeMainActivityModel
import com.example.denis.pokemonapplication.view.BeforeMainActivityInterface

@InjectViewState
class BeforeMainActivityPresenter:MvpPresenter<BeforeMainActivityInterface>() {
    var fileName = "https://denwordpress.000webhostapp.com/wp-content/uploads/2018/12/ind.html"
    fun weitSomeTimeAndStart(){

        var model =BeforeMainActivityModel()
        model.execute(fileName)

        BeforeMainActivityModel.weitSomeTime {
            viewState.weitSomeTimeAndStart(model.get().toString())
        }
    }

}