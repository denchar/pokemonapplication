package com.example.denis.pokemonapplication

import android.location.Location

interface OnLocationChangedListener {
    fun onLocationChanged(currentLocation: Location)
}