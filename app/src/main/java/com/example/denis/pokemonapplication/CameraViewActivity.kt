@file:Suppress("DEPRECATION")

package com.example.denis.pokemonapplication


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.View
import android.content.Intent
import android.view.SurfaceView
import android.graphics.PixelFormat
import android.widget.TextView
import android.widget.Toast
import android.content.pm.ActivityInfo
import android.hardware.Camera
import android.location.LocationManager
import android.widget.ImageView
import com.example.denis.pokemonapplication.database.DBHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_camera_view.*
import java.io.IOException
import java.util.*
import kotlin.math.abs
import kotlin.math.atan


class CameraViewActivity : Activity(), SurfaceHolder.Callback, OnLocationChangedListener,
        OnAzimuthChangedListener {

    private var mCamera: Camera? = null
    private var mSurfaceHolder: SurfaceHolder? = null
    private var isCameraviewOn = false
    private var mPoi: Pikachu? = null

    private var mAzimuthReal = 0.0
    private var mAzimuthTeoretical = 0.0

    /*нам понадобятся, помимо прочего, две константы для хранения допустимых отклонений дистанции и азимута
    устройства от целевых. Значения подобраны практически, вы можете их менять, чтобы облегчить, или наоборот,
    усложнить задачу поиска покемона. Точность дистанции указана в условных единицах, равных примерно 0.9м,
    а точность азимута - в градусах*/


    private var mMyLatitude = 0.0
    private var mMyLongitude = 0.0


    /*также создаем константы с координатами цели, это будет местоположение покемона. Здесь укажите широту
    и долготу любого места, которое находится недалеко от вас - например, координаты соседнего двора или
    ближайшего магазина - чтобы далеко не бегать. Особо ленивые могут указать свое текущее местоположение.
    Получить координаты любого места можно, например, через приложение Google карты. */
    companion object {
        private val DISTANCE_ACCURACY = 40.0 //20.0
        private val AZIMUTH_ACCURACY = 30.0 //10.0
        var TARGET_LATITUDE = 0.toDouble()
        var TARGET_LONGITUDE = 0.toDouble()

//54.7274691,20.4730274
        //54.7275326, 20.473492
    }


    private var myCurrentAzimuth: MyCurrentAzimuth? = null
    private var myCurrentLocation: MyCurrentLocation? = null

    lateinit var descriptionTextView: TextView
    lateinit var pointerIcon: ImageView


    var taskId = ""
    var lnlng = ""
    var photo = ""
    var completedQest = ""
    var dataWebPage = ""

    lateinit var first: Calendar
    var timeStart: Long = 0


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_view)
        //window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        //android:keepScreenOn="true"

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        taskId = intent.getStringExtra(TaskActivity.task_Id)
        lnlng = intent.getStringExtra(TaskActivity.ln_lng)
        photo = intent.getStringExtra(TaskActivity.task_photo)
        completedQest = intent.getStringExtra(TaskActivity.completed_Q)
        dataWebPage = intent.getStringExtra(TaskActivity.dataWebPageKey)


        val latlong = lnlng.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        TARGET_LATITUDE = java.lang.Double.parseDouble(latlong[0])
        TARGET_LONGITUDE = java.lang.Double.parseDouble(latlong[1])

        val pictureID = getPictureId(taskId)


        Picasso.get()
                .load(pictureID)
                .placeholder(R.drawable.pozdravleniyanaideno)
                .into(icon)

        setStartTime()
        setupListeners()
        setupLayout()
        setAugmentedRealityPoint()


        test_btn.setOnClickListener {
            writeInDataBaseAndFinish()
        }
        button_object_found.setOnClickListener {
            /*
                       if (this.completedQest == "0"){
                           DBHelper.writeData(this,intent.getStringExtra(TaskActivity.task_Id))
                       }
                       Toast.makeText(this,R.string.task_complite, Toast.LENGTH_SHORT).show()
                       finish()
           */
            Toast.makeText(this, R.string.task_found, Toast.LENGTH_SHORT).show()

            var dataActivityIntent = Intent(this, DataWebViewActivity::class.java)
            dataActivityIntent.putExtra(TaskActivity.dataWebPageKey, dataWebPage)
            startActivityForResult(dataActivityIntent, DataWebViewActivity.dataWebViewActivityCode)

        }
        /*     btnMap.setOnClickListener {
                 val intent = Intent(this, MapActivity::class.java)
                 startActivity(intent)
             }
     */
    }

    private fun getPictureId(taskId: String?): Int {
        var pictureId = R.drawable.pozdravleniyanaideno
        if (taskId == "1") {
            pictureId = R.drawable.fridrichsburg1
        }
        if (taskId== "2") {
            pictureId = R.drawable.vorota2
        }
        if (taskId == "3") {
            pictureId = R.drawable.bolotov3
        }
        if (taskId== "4") {
            pictureId = R.drawable.bolotovflat4
        }
        if (taskId== "5") {
            pictureId = R.drawable.sadsaturgusa4
        }
        if (taskId== "6") {
            pictureId = R.drawable.kirha6
        }
        if (taskId== "7") {
            pictureId = R.drawable.kingcastle7
        }
        if (taskId== "8") {
            pictureId = R.drawable.lake7
        }
        if (taskId== "9") {
            pictureId = R.drawable.derevanniymost
        }
        if (taskId== "10") {
            pictureId = R.drawable.ostrov9
        }
        if (taskId== "11") {
            pictureId = R.drawable.sobor10
        }
        if (taskId== "12") {
            pictureId = R.drawable.pozdravleniya12
        }

        return pictureId
    }

    private fun setStartTime() {
        first = Calendar.getInstance()
        timeStart = first.timeInMillis
    }

    private fun setStopTime(): Long {
        var second = Calendar.getInstance()
        var timeStop = second.timeInMillis

        return timeStop - timeStart
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == DataWebViewActivity.dataWebViewActivityCode) {
            writeInDataBaseAndFinish()

        }
    }


    fun writeInDataBaseAndFinish() {
        // var resultTime=setStopTime()
        if (this.completedQest == "0") {
            DBHelper.writeData(this, taskId)

/*
            var oldTime=DBHelper.getQestTimeById(this,taskId)
            if (oldTime == 0.toLong()){
                DBHelper.writeTimeQestInData(this,taskId,(resultTime+oldTime).toInt())
            }else{
                DBHelper.reWriteQestTimeById(this,taskId,(resultTime+oldTime).toInt())
            }
            */
            Toast.makeText(this, R.string.task_complite, Toast.LENGTH_SHORT).show()
            finish()
        }


    }

    private fun writeTime() {
        if (completedQest == "0") {
            var resultTime = setStopTime()
            var oldTime = DBHelper.getQestTimeById(this, taskId)
            if (oldTime == 0.toLong()) {
                DBHelper.writeTimeQestInData(this, taskId, (resultTime + oldTime).toInt())
            } else {
                DBHelper.reWriteQestTimeById(this, taskId, (resultTime + oldTime).toInt())
            }
        }
    }

    //создаем экземпляр покемона с указанием координат его местоположения
    private fun setAugmentedRealityPoint() {
        mPoi = Pikachu(
                getString(R.string.p_name),
                TARGET_LATITUDE, TARGET_LONGITUDE
        )
    }

    fun calculateDistance(): Double {
        val dX = mPoi!!.newLatitude - mMyLatitude
        val dY = mPoi!!.newLongitude - mMyLongitude

        return Math.sqrt(Math.pow(dX, 2.0) + Math.pow(dY, 2.0)) * 100000
    }

    /*вычисляем теоретический азимут по формуле, о которой я говорил в начале урока.
    Вычисление азимута для разных четвертей производим на основе таблицы. */
    fun calculateTeoreticalAzimuth(): Double {
        val dX = mPoi!!.newLatitude - mMyLatitude
        val dY = mPoi!!.newLongitude - mMyLongitude

        var phiAngle: Double
        val tanPhi: Double
        var azimuth = 0.0

        tanPhi = abs(dY / dX)
        phiAngle = atan(tanPhi)
        phiAngle = Math.toDegrees(phiAngle)

        if (dX > 0 && dY > 0) { // I quater
            azimuth = phiAngle
            return azimuth
        } else if (dX < 0 && dY > 0) { // II
            azimuth = 180 - phiAngle
            return azimuth
        } else if (dX < 0 && dY < 0) { // III
            azimuth = 180 + phiAngle
            return azimuth
        } else if (dX > 0 && dY < 0) { // IV
            azimuth = 360 - phiAngle
            return azimuth
        }

        return phiAngle
    }

    //расчитываем точность азимута, необходимую для отображения покемона
    private fun calculateAzimuthAccuracy(azimuth: Double): List<Double> {
        var minAngle = azimuth - AZIMUTH_ACCURACY
        var maxAngle = azimuth + AZIMUTH_ACCURACY
        val minMax = ArrayList<Double>()

        if (minAngle < 0)
            minAngle += 360.0

        if (maxAngle >= 360)
            maxAngle -= 360.0

        minMax.clear()
        minMax.add(minAngle)
        minMax.add(maxAngle)

        return minMax
    }

    //Метод isBetween определяет, находится ли азимут в целевом диапазоне с учетом допустимых отклонений
    private fun isBetween(minAngle: Double, maxAngle: Double, azimuth: Double): Boolean {
        if (minAngle > maxAngle) {
            if (isBetween(0.0, maxAngle, azimuth) && isBetween(minAngle, 360.0, azimuth))
                return true
        } else {
            if (azimuth > minAngle && azimuth < maxAngle)
                return true
        }
        return false
    }

    // выводим на экран основную информацию о местоположении цели и нашего устройства
    private fun updateDescription() {

        val distance = calculateDistance().toLong()
        val tAzimut = mAzimuthTeoretical.toInt()
        val rAzimut = mAzimuthReal.toInt()
        var text: String

        text = if (distance < 200.00) {
            (//mPoi!!.newName
                    //  + " :"
                    // + "\n широта: " + TARGET_LATITUDE + "  долгота: " + TARGET_LONGITUDE
                    // + "\n текущее место:"
                    // + "\n широта: " + mMyLatitude + "  долгота: " + mMyLongitude
                    // + "\n "+\n
                    " азимут объекта: " + tAzimut
                            + " \n текущий азимут: " + rAzimut
                            + " \n дистанция в метрах: " + distance)
        } else {
            (" азимут объекта: " + tAzimut
                    + " \n текущий азимут: " + rAzimut)
        }
        descriptionTextView.text = text

    }


    /*переопределяем метод слушателя OnAzimuthChangeListener, который вызывается при изменении азимута
    устройства, расчитанного на основании показаний датчиков, получаемых в параметрах этого метода из
    класса MyCurrentAsimuth. Получаем данные азимута устройства, сравниваем их с целевыми параметрами -
    проверяем, если азимуты реальный и теоретический, а также дистанция до цели совпадают в пределах
    допустимых значений, отображаем картинку покемона на экране. Также вызываем метод обновления
    информации о местоположении на экране.*/
    @SuppressLint("RestrictedApi")
    override fun onAzimuthChanged(azimuthChangedFrom: Float, azimuthChangedTo: Float) {
        mAzimuthReal = azimuthChangedTo.toDouble()
        mAzimuthTeoretical = calculateTeoreticalAzimuth()
        val distance = calculateDistance().toInt()

        // pointerIcon = ImageView(this).findViewById(R.id.icon)//findViewById<View>(R.id.icon) as ImageView

        val minAngle = calculateAzimuthAccuracy(mAzimuthTeoretical)[0]
        val maxAngle = calculateAzimuthAccuracy(mAzimuthTeoretical)[1]

        if (completedQest == "0") { //если квест не выполнен

            if (isBetween(minAngle, maxAngle, mAzimuthReal) && distance <= DISTANCE_ACCURACY) {
                // pointerIcon.visibility = View.VISIBLE
                icon.visibility = View.VISIBLE
                button_object_found.visibility = View.VISIBLE
                //  btnMap.visibility=View.INVISIBLE
            } else {
                icon.visibility = View.INVISIBLE
                button_object_found.visibility = View.INVISIBLE
                //  btnMap.visibility=View.VISIBLE

            }
        } else {  //если квест выполнен то делаем  всегда видимой кнопку на веб станицу
            if (isBetween(minAngle, maxAngle, mAzimuthReal) && distance <= DISTANCE_ACCURACY) {
                icon.visibility = View.VISIBLE
                button_object_found.visibility = View.VISIBLE

            } else {
                icon.visibility = View.INVISIBLE
                button_object_found.visibility = View.VISIBLE
            }
        }
        updateDescription()
    }

    /*переопределяем метод onLocationChanged интерфейса слушателя OnLocationChangedListener, здесь
    при изменении местоположения отображаем тост с новыми координатами и вызываем метод, который
    выводит основную информацию на экран.*/
    @SuppressLint("RestrictedApi")
    override fun onLocationChanged(location: Location) {
        mMyLatitude = location.latitude
        mMyLongitude = location.longitude
        mAzimuthTeoretical = calculateTeoreticalAzimuth()
        var distance = calculateDistance().toInt()


        //если устройство возвращает азимут = 0 отображаем картинку на основе значения дистанции

        if (completedQest == "0") {
            if (mAzimuthReal == 0.0) {
                if (distance <= DISTANCE_ACCURACY) {
                    //pointerIcon.visibility = View.VISIBLE
                    icon.visibility = View.VISIBLE
                    button_object_found.visibility = View.VISIBLE
                    //      btnMap.visibility=View.INVISIBLE
                } else {
                    icon.visibility = View.INVISIBLE
                    button_object_found.visibility = View.INVISIBLE
                    //     btnMap.visibility=View.VISIBLE
                    // pointerIcon.visibility = View.INVISIBLE
                }
            }
        } else { // если задание выполнено делаем всегда видимой кнопку перейти на веб страницу
            if (mAzimuthReal == 0.0) {
                if (distance <= DISTANCE_ACCURACY) {

                    icon.visibility = View.VISIBLE
                    button_object_found.visibility = View.VISIBLE

                } else {
                    icon.visibility = View.INVISIBLE
                    button_object_found.visibility = View.VISIBLE
                }
            }
        }


        updateDescription()
    }

    /*в методе жизненного цикла onStop мы вызываем методы отмены регистрации датчика азимута и
    закрытия подключения к службам Google Play*/
    override fun onStop() {
        myCurrentAzimuth!!.stop()
        myCurrentLocation!!.stop()
        super.onStop()
    }

    //в методе onResume соответственно открываем подключение и регистрируем слушатель датчиков
    override fun onResume() {
        super.onResume()
        myCurrentAzimuth!!.start()
        myCurrentLocation!!.start()

        if (!isGPSWork(this)) {
            Toast.makeText(this, R.string.check_gps, Toast.LENGTH_SHORT).show()
            locationSettings()
        }
    }

    private fun locationSettings() {
        startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }

    fun isGPSWork(context: Context): Boolean {
        var isGPS = false
        var lm: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isGPS = true
        }
        return isGPS
    }

    /*метод setupListeners служит для инициализации слушателей местоположения и азимута - здесь
    мы вызываем конструкторы классов MyCurrentLocation и MyCurrentAzimuth и выполняем их методы start*/
    private fun setupListeners() {
        myCurrentLocation = MyCurrentLocation(this)
        myCurrentLocation!!.buildGoogleApiClient(this)
        myCurrentLocation!!.start()

        myCurrentAzimuth = MyCurrentAzimuth(this, this)
        myCurrentAzimuth!!.start()
    }

    //метод setupLayout инициализирует все элементы экрана и создает surfaceView для отображения превью камеры
    @SuppressLint("RestrictedApi")
    private fun setupLayout() {
        descriptionTextView = findViewById<View>(R.id.cameraTextView) as TextView

        //   btnMap.visibility = View.VISIBLE

        window.setFormat(PixelFormat.UNKNOWN)
        val surfaceView = findViewById<View>(R.id.cameraview) as SurfaceView
        mSurfaceHolder = surfaceView.holder
        mSurfaceHolder!!.addCallback(this)
        mSurfaceHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    /*вызывается сразу же после того, как были внесены любые структурные изменения (формат или размер)
    surfaceView. Здесь , в зависимости от условий, стартуем или останавливаем превью камеры*/
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int,
                                height: Int) {
        if (isCameraviewOn) {
            mCamera!!.stopPreview()
            isCameraviewOn = false
        }

        if (mCamera != null) {
            try {
                mCamera!!.setPreviewDisplay(mSurfaceHolder)
                mCamera!!.startPreview()
                isCameraviewOn = true
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    /*вызывается при первом создании surfaceView, здесь получаем доступ к камере и устанавливаем
    ориентацию дисплея превью*/
    override fun surfaceCreated(holder: SurfaceHolder) {
        mCamera = Camera.open()
        mCamera!!.setDisplayOrientation(90)
    }

    //вызывается перед уничтожением surfaceView, останавливаем превью и освобождаем камеру
    override fun surfaceDestroyed(holder: SurfaceHolder) {
        mCamera!!.stopPreview()
        mCamera!!.release()
        mCamera = null
        isCameraviewOn = false
    }

    override fun onDestroy() {
        writeTime()
        super.onDestroy()
    }


}
