package com.example.denis.pokemonapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.denis.pokemonapplication.database.DBHelper
import kotlinx.android.synthetic.main.activity_result.*
import kotlinx.android.synthetic.main.content_result.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        setSupportActionBar(toolbar)

        var taskList=DBHelper.getData(this)
        var list=DBHelper.getAllQestTime(this)
        var string=""
        var taskNumber=0
        var time=0
        while(taskNumber<list.size){

            var idTime=""
            var status=""
            if (taskList.size>taskNumber){
                status=" выполнено. "}
            if(list.size>taskNumber){
                time= list[taskNumber]
                idTime =formateDate(time.toLong())
            }
              string+=" \n Задание "+(taskNumber+1) +status+
                      " \n Время "+idTime +" (мин:сек) \n"
             taskNumber++
        }
        text_view_result_time.text=string

        var s = "Итог: Заданий выполнено "+taskList.size.toString()+"\n Общее время "+getAllTime(list)
        all_result_text_view.text=s
    }
    fun getAllTime(list:ArrayList<Int>):String{

       var m=0
        for (item in list){
            m += item
        }
        return formateDate(m.toLong())
    }

    fun formateDate(time:Long):String{
        var simpleDateFormat = SimpleDateFormat(
                " mmm:ss ",
                Locale.getDefault()
        )
        return simpleDateFormat.format(time)
    }
}
