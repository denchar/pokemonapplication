package com.example.denis.pokemonapplication.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface TaskActivityInterface :MvpView{

    @StateStrategyType(SkipStrategy::class)
    fun writeTaskStatus(string: String,status:String)

}