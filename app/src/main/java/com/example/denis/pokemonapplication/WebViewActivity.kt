package com.example.denis.pokemonapplication

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import android.webkit.WebViewClient

class WebViewActivity : AppCompatActivity() {
    private lateinit var webPage: WebView
    private lateinit var mUrl: String

    @SuppressLint("SetJavaScriptEnabled")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

         mUrl= intent.getStringExtra(TaskActivity.task_web_page)

            webPage = findViewById<WebView>(R.id.webPage)
            webPage.settings.javaScriptEnabled = true
            webPage.settings.builtInZoomControls = true
            webPage.webViewClient = MyWebViewClient()

            //webPage.loadUrl(mUrl)
        webPage.loadUrl("https://ru.wikipedia.org/wiki/Заглавная_страница")
    }

    private inner class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }
}
