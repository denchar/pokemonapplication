package com.example.denis.pokemonapplication.presenter


import android.content.Context

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.denis.pokemonapplication.database.DBHelper
import com.example.denis.pokemonapplication.model.ModelClass
import com.example.denis.pokemonapplication.model.ModelReadFileFromDisc
import com.example.denis.pokemonapplication.recyclerViewTask.QestTask
import com.example.denis.pokemonapplication.view.ReadFileFromDiskInterface

@InjectViewState
class PresenterReadFileFromDisc: MvpPresenter<ReadFileFromDiskInterface>() {

    var modelClass=ModelClass()
    var textTask=""
    var list=ArrayList<QestTask>()
    var taskStatusList=ArrayList<String>()
   // var fileName ="https://denwordpress.000webhostapp.com/wp-content/uploads/2018/08/ind.html"

    fun startReadFileFrom(context: Context,string:String){

       // var model=ModelReadFileFromDisc()
       // model.execute(fileName)
       // textTask=model.get().toString()
         textTask=string
        taskStatusList =getDataFromDatabase(context)


        if (textTask.length<2){
            viewState.messageError()
        } else {
            list = modelClass.putQestInList(textTask)
            viewState.qestListForRecyclerView(list,taskStatusList)
        }
    }

    fun getDataFromDatabase(context :Context):ArrayList<String>{

        return DBHelper.getData(context)

    }
    fun deleteDataInDB(context: Context){

        DBHelper.deleteData(context)
        viewState.reloadAfterDeleteData()

    }

}