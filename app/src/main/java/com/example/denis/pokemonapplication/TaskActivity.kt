package com.example.denis.pokemonapplication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter

import com.example.denis.pokemonapplication.presenter.TaskActivityPresenter
import com.example.denis.pokemonapplication.view.TaskActivityInterface
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.android.synthetic.main.content_task.*


class TaskActivity : MvpAppCompatActivity(), TaskActivityInterface {


    @InjectPresenter
    lateinit var tAPresenter: TaskActivityPresenter


   // var completedQest=""

    companion object {

        const val task_Id="taskId"
        const val task_Type="taskType"
        const val ln_lng="lnlng"
        const val task_title="textTitle"
        const val task_Text="taskText"
        const val task_photo="photo"
        const val completed_Q="completedQest"
        const val task_web_page="taskWebPage"
        const val dataWebPageKey="datawebpage"




        var taskString = "Задание "
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
       // setSupportActionBar(ttoolbar)

        findViewById<TextView>(R.id.activity_task_number).text= taskString+intent.getStringExtra(TaskActivity.task_Id)
        findViewById<TextView>(R.id.activity_task_text).text="  "+intent.getStringExtra(TaskActivity.task_Text)

/*
        fab.setOnClickListener {
            view ->
            var nIntent = Intent(this,CameraViewActivity::class.java)
            nIntent.putExtra(TaskActivity.task_Id,intent.getStringExtra(TaskActivity.task_Id))
            nIntent.putExtra(TaskActivity.ln_lng,intent.getStringExtra(TaskActivity.ln_lng))
            nIntent.putExtra(TaskActivity.task_title,intent.getStringExtra(TaskActivity.task_title))
            nIntent.putExtra(TaskActivity.task_photo,intent.getStringExtra(TaskActivity.task_photo))
            nIntent.putExtra(TaskActivity.completed_Q,intent.getStringExtra(TaskActivity.completed_Q))
            nIntent.putExtra(TaskActivity.dataWebPageKey,intent.getStringExtra(TaskActivity.dataWebPageKey))
            startActivity(nIntent)
        }
*/


       // findViewById<TextView>(R.id.activity_task_type).text=intent.getStringExtra(TaskActivity.task_Type)
        //activity_task_lnlng.text=intent.getStringExtra(TaskActivity.ln_lng)
       // Picasso.get().load(intent.getStringExtra(TaskActivity.photo)).into(image_view_task)
       // completedQest=intent.getStringExtra(TaskActivity.completed_Q)
        // trueResponse=intent.getStringExtra("taskResponse")


    }

    override fun onResume() {
        super.onResume()
        tAPresenter.getTaskStatus(this,intent.getStringExtra(TaskActivity.task_Id))
    }


    override fun writeTaskStatus(string: String,status:String) {
        task_status.text=string

        fab.setOnClickListener {
            view ->
            var nIntent = Intent(this,CameraViewActivity::class.java)
            nIntent.putExtra(TaskActivity.task_Id,intent.getStringExtra(TaskActivity.task_Id))
            nIntent.putExtra(TaskActivity.ln_lng,intent.getStringExtra(TaskActivity.ln_lng))
            nIntent.putExtra(TaskActivity.task_title,intent.getStringExtra(TaskActivity.task_title))
            nIntent.putExtra(TaskActivity.task_photo,intent.getStringExtra(TaskActivity.task_photo))
            nIntent.putExtra(TaskActivity.completed_Q,status)
            nIntent.putExtra(TaskActivity.dataWebPageKey,intent.getStringExtra(TaskActivity.dataWebPageKey))

            startActivity(nIntent)
        }

    }
}
