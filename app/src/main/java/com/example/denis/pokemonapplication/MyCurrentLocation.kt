package com.example.denis.pokemonapplication

import android.annotation.SuppressLint
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


class MyCurrentLocation (private var onLocationChangedListener: OnLocationChangedListener): GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener, LocationListener {



    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null
    private var mLocationRequest: LocationRequest? = null


    //передаем интерфейс OnLocationChangedListener в конструкторе для организации
    //прослушивания события смены местоположения


    /**
     * Создает GoogleApiClient. Использует метод { @code #addApi} для запроса
     * LocationServices API.
     */
    @Synchronized
    fun buildGoogleApiClient(context: Context) {
        mGoogleApiClient = GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        //создаем запрос и устанавливаем интервал для его отправки
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000) // 1 second, in milliseconds
    }

    fun start() {
        //Подключает клиента к службам Google Play.
        mGoogleApiClient!!.connect()
    }

    fun stop() {
        //Закрывает подключение к службам Google Play.
        mGoogleApiClient!!.disconnect()
    }

    //После вызова connect(), этот метод будет вызываться асинхронно после успешного завершения запроса подключения.
    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient)
        if (mLastLocation != null) {
            onLocationChangedListener.onLocationChanged(mLastLocation!!)
        }
    }

    //Вызывается, когда клиент временно в отключенном состоянии.
    override fun onConnectionSuspended(i: Int) {

    }

    //Вызывается, когда произошла ошибка при подключении клиента к службе.
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("MyApp", "Location services connection failed with code " + connectionResult.getErrorCode())
    }

    /*
     * Реализуем метод onLocationChanged интерфейса LocationListener. Обратный вызов,
который возникает, когда изменяется местоположение.
     * Здесь создаем объект mLastLocation, который хранит последнее местоположение и передаем его в методе интерфейса.
     */
    @SuppressLint("MissingPermission")
    override fun onLocationChanged(location: Location) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient)
        if (mLastLocation != null) {
            onLocationChangedListener.onLocationChanged(mLastLocation!!)
        }
    }
}