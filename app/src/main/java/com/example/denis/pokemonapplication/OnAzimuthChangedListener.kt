package com.example.denis.pokemonapplication

interface OnAzimuthChangedListener {
    fun onAzimuthChanged(azimuthFrom: Float, azimuthTo: Float)
}