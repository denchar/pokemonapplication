package com.example.denis.pokemonapplication

import android.Manifest
import android.annotation.SuppressLint
import android.app.Instrumentation
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.denis.pokemonapplication.presenter.PresenterReadFileFromDisc
import com.example.denis.pokemonapplication.recyclerViewTask.QestTask
import com.example.denis.pokemonapplication.recyclerViewTask.TaskAdapter
import com.example.denis.pokemonapplication.view.ReadFileFromDiskInterface
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.content_scrolling_activity_main.*


class MainActivity : MvpAppCompatActivity(),ReadFileFromDiskInterface {


    @InjectPresenter
    lateinit var presenter:PresenterReadFileFromDisc
     lateinit var ad: AlertDialog.Builder
    lateinit var completeDialogQest:AlertDialog.Builder

    var taskList=ArrayList<QestTask>()

   var page = ""

    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mtoolbar)

        page=intent.getStringExtra(BeforeMainActivity.pageContent)
/*
        button_to_map.setOnClickListener(View.OnClickListener {
            var intent= Intent(this,CameraViewActivity::class.java)
            intent.putExtra(TaskActivity.task_photo,"https://denwordpress.000webhostapp.com/wp-content/uploads/2018/08/fridrichsburg1.jpg")
            intent.putExtra(TaskActivity.ln_lng, "54.7275326, 20.473492")
            intent.putExtra(TaskActivity.task_Id,"0")
            intent.putExtra(TaskActivity.completed_Q,"1")
            startActivity(intent)
        })
      */
        error_button.setOnClickListener {
            var nIntent = Intent(this,BeforeMainActivity::class.java)
            startActivity(nIntent)
            error_button.visibility=View.GONE
            finish()
        }

        /*     if (!isOnline(this)) {
                 Toast.makeText(this,"Проверьте подключение к интернету ",Toast.LENGTH_SHORT).show()
                 networkSettings()
             }
             */
        if (!isGPSWork(this)){
            Toast.makeText(this,R.string.check_gps,Toast.LENGTH_SHORT).show()
            locationSettings()
        }

        recycler_task_list.layoutManager= GridLayoutManager(applicationContext,1)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


// обработать и включить !!!!!!!!
    private fun locationSettings() {
        startActivity( Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }
  /*  private fun networkSettings() {

        startActivity( Intent(android.provider.Settings.ACTION_NETWORK_OPERATOR_SETTINGS))
    }
    */
    fun readFile(string:String){
        presenter.startReadFileFrom(applicationContext,string)
    }
    override fun reloadAfterDeleteData() {
        onResume()
    }
    override fun onResume() {
        super.onResume()
        readFile(page)
    }
    override fun writeTextInTextView(string:String) {

    }
    override fun qestListForRecyclerView(list:ArrayList<QestTask>, taskStatusList:ArrayList<String>) {
        taskList=list
        recycler_task_list.adapter=TaskAdapter(taskList,this,taskStatusList)
    }
    override fun messageError() {

        error_text_view.visibility=View.VISIBLE
        error_text_view.text="Ошибка! Перезапустите приложение!"
        error_button.visibility=View.VISIBLE
    }
/*
    fun isOnline(context: Context): Boolean {
        var network=false
        var cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var netInfo = cm.activeNetworkInfo


        if (netInfo !=null && netInfo.isConnectedOrConnecting ){
            network=true
        }
        return network
    }
*/
    fun isGPSWork(context: Context):Boolean{
        var isGPS=false
        var lm: LocationManager =  context.getSystemService( Context.LOCATION_SERVICE ) as LocationManager

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            isGPS=true
        }
        return isGPS
    }

     override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        // Операции для выбранного пункта меню
        when (id) {
            R.id.action_delete_data_in_database -> {

                var message = "Обнулить результат?"
                var button1String ="Да"
                var button2String = "Отмена"

                ad = AlertDialog.Builder(this)

                ad.setMessage(message) // сообщение
                ad.setPositiveButton(button1String) { dialog, arg1 ->

                    presenter.deleteDataInDB(this)
                }
                ad.setNegativeButton(button2String) { dialog, arg1 -> }
                ad.setCancelable(true)
                ad.setOnCancelListener { }
                ad.show()
            }
            R.id.action_complete_qest ->{
                var message = "Завершить квест?"
                var button1String ="Да"
                var button2String = "Отмена"
                completeDialogQest=AlertDialog.Builder(this)
                        .setMessage(message)
                        .setPositiveButton(button1String){ _, _ ->
                            var resultIntent=Intent(this, ResultActivity::class.java)
                            startActivity(resultIntent)
                        }
                        .setNegativeButton(button2String){ _, _ ->  }
                        .setCancelable(true)
                        .setOnCancelListener {  }
                       completeDialogQest.show()
            }
        }

        return true
    }
}
