package com.example.denis.pokemonapplication.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DBHelper( context:Context, name: String, factory: SQLiteDatabase.CursorFactory?, version: Int)
    : SQLiteOpenHelper(context, name, factory, version) {



    override fun onCreate(db: SQLiteDatabase?) {

        db!!.execSQL("create table " + TABLE_QESTS + "(" + KEY_ID
                + " integer primary key," + KEY_STATUS + " text" + ")")

        db.execSQL("create table "
                + TABLE_TOTAL_TIME + "("
                + KEY_TIME_ID+" integer primary key, "
                + KEY_QEST_ID+" text, "
                + KEY_SEPARATE_QEST_TIME +" integer) ")
    }


    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        db!!.execSQL("drop table if exists " + TABLE_QESTS)
        db.execSQL("drop table if exists $TABLE_TOTAL_TIME")
        onCreate(db)



    }

    companion object {
        val DATABASE_VERSION = 2
        val DATABASE_NAME = "contactDb"
        val TABLE_QESTS = "qests"
        val KEY_ID = "_id"
        val KEY_STATUS= "status"

        val KEY_TIME_ID = "_id"
        val TABLE_TOTAL_TIME="totalTime"
        val KEY_QEST_ID="qestId"
        val KEY_SEPARATE_QEST_TIME="separateQestTime"

    fun getData(context: Context):ArrayList<String>{ // не очень элегантно. изменить. 2 соединения с бд

        var list=ArrayList<String>()
        var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)

        var db=dbHelper.writableDatabase

        var cursor=db.query(DBHelper.TABLE_QESTS,null,null,null,null,null,null)

        if (cursor.moveToFirst()) {
            val keyId = cursor.getColumnIndex(DBHelper.KEY_ID)
            val keyStatus = cursor.getColumnIndex(DBHelper.KEY_STATUS)
            do {
                Log.d("mLog", "ID = " + cursor.getInt(keyId) + " status = "+ cursor.getInt(keyStatus))
                list.add(cursor.getInt(keyStatus).toString())

            } while (cursor.moveToNext())
        } else{
            Log.d("mLog", "0 rows - getData")}


        cursor.close()
        dbHelper.close()
        db.close()
        return list

    }
    fun writeData(context: Context,taskId:String){

        var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
        var db=dbHelper.writableDatabase
        var contentValues = ContentValues()
        contentValues.put(DBHelper.KEY_STATUS, taskId )
        db.insert(DBHelper.TABLE_QESTS, null, contentValues)
        contentValues.clear()

        dbHelper.close()
        db.close()

    }
        fun writeTimeQestInData(context: Context,taskId:String,qestTime: Int){
            var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
            var db=dbHelper.writableDatabase
            var contentValues = ContentValues()
            contentValues.put(DBHelper.KEY_QEST_ID,taskId)
            contentValues.put(DBHelper.KEY_SEPARATE_QEST_TIME,qestTime)
            db.insert(DBHelper.TABLE_TOTAL_TIME,null,contentValues)
            contentValues.clear()

            dbHelper.close()
            db.close()
        }
        fun getQestTimeById(context: Context,taskId:String):Long{

            var list=ArrayList<Long>()
            var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
            var db=dbHelper.writableDatabase
            var cursor=db.query(DBHelper.TABLE_TOTAL_TIME,arrayOf(DBHelper.KEY_ID,
                    DBHelper.KEY_QEST_ID,DBHelper.KEY_SEPARATE_QEST_TIME),DBHelper.KEY_QEST_ID+"=?",
                    arrayOf(taskId),null,null,null)

            if (cursor.moveToFirst()) {

                val keySeparateQestTime=cursor.getColumnIndex(DBHelper.KEY_SEPARATE_QEST_TIME)
                do {
                    Log.d( "mLog","get_by_id_time = "+ cursor.getInt(keySeparateQestTime))

                    list.add(cursor.getInt(keySeparateQestTime).toLong())

                } while (cursor.moveToNext())
            } else{
                Log.d("mLog", "0 rows - getQestTimeById")
                list.add(0.toLong())
            }

            var time=0.toLong()
            for (item in list){
                time=+item
            }

            cursor.close()
            dbHelper.close()
            db.close()
            return time.toLong()
        }
        fun reWriteQestTimeById(context: Context,taskId:String,qestTime: Int){
            var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
            var db=dbHelper.writableDatabase
            var contentValues = ContentValues()
            contentValues.put(DBHelper.KEY_QEST_ID,taskId)
            contentValues.put(DBHelper.KEY_SEPARATE_QEST_TIME,qestTime)
            db.update(DBHelper.TABLE_TOTAL_TIME,contentValues,DBHelper.KEY_QEST_ID +"= ?",arrayOf<String>(taskId))
            contentValues.clear()

            dbHelper.close()
            db.close()
        }
        fun getAllQestTime(context: Context):ArrayList<Int>{
            var list=ArrayList<Int>()
            var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
            var db=dbHelper.writableDatabase
            var cursor=db.query(DBHelper.TABLE_TOTAL_TIME,null,null,null,null,null,null)

            if (cursor.moveToFirst()) {
                val keyId = cursor.getColumnIndex(DBHelper.KEY_ID)
                val keyQestId = cursor.getColumnIndex(DBHelper.KEY_QEST_ID)
                val keySeparateQestTime=cursor.getColumnIndex(DBHelper.KEY_SEPARATE_QEST_TIME)
                do {
                    Log.d("mLog", "ID = " + cursor.getInt(keyId) +
                            " qest_id = "+ cursor.getInt(keyQestId)+
                    "separate_qest_time = "+ cursor.getInt(keySeparateQestTime))

                    list.add(cursor.getInt(keySeparateQestTime))

                } while (cursor.moveToNext())
            } else{
                Log.d("mLog", "0 rows - getAllQestTime")}


            cursor.close()
            dbHelper.close()
            db.close()
            return list
        }

        fun deleteData(context: Context){
            var dbHelper =  DBHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION)
            var db=dbHelper.writableDatabase
            db.delete(DBHelper.TABLE_QESTS, null, null)
            db.delete(DBHelper.TABLE_TOTAL_TIME,null,null)

            dbHelper.close()
            db.close()

        }
    }

}