package com.example.denis.pokemonapplication.presenter

import android.content.Context
import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.denis.pokemonapplication.R
import com.example.denis.pokemonapplication.database.DBHelper
import com.example.denis.pokemonapplication.view.TaskActivityInterface

@InjectViewState
class TaskActivityPresenter :MvpPresenter<TaskActivityInterface>(){

    fun getTaskStatus(context: Context,string: String){
        var taskStatusList= DBHelper.getData(context)

        println( "size= "+taskStatusList.size +" id= "+string)
        if (taskStatusList.size >= string.toInt()){
            viewState.writeTaskStatus("Задание выполнено","1")

        }else{
            viewState.writeTaskStatus("Задание выполняется","0")

        }

    }

}