package com.example.denis.pokemonapplication.model

import com.example.denis.pokemonapplication.recyclerViewTask.QestTask
import com.google.android.gms.maps.model.LatLng

import org.json.JSONArray
import org.json.JSONObject

class ModelClass {

    fun putQestInList( string :String):ArrayList<QestTask>{

        var list=ArrayList<QestTask>()

        var jsonObject = JSONObject(string)
        var ja: JSONArray = jsonObject.getJSONArray("tasks")
        var text =""
        for (i in 1..ja.length()){
            var obj:JSONObject= ja.getJSONObject(i-1)

            var taskId:Int=obj.getInt("task_id")
            var taskType:String=obj.getString("task_type")
            var task=  obj.getJSONObject("task")
            var lnlng:String= task.getString("lnlng")
            var taskTitle:String =task.getString("task_title")
            var taskText:String=task.getString("task_text")
            var photoUrl:String=task.getString("photo")
            var dataWebPage:String=task.getString("data_page")
            var qest=QestTask(taskId,taskType,lnlng,taskTitle,taskText,photoUrl,dataWebPage)
            list.add(qest)
        }

        return list
    }
/*
    fun stringToLatLng(string: String):LatLng{
        var latlong = string.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var latitude = java.lang.Double.parseDouble(latlong[0])
        var longitude = java.lang.Double.parseDouble(latlong[1])
        var location = LatLng(latitude, longitude)
        return location
    }
    */
}